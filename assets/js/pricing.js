
	$(document).ready(function() {
		switchOptions = function(selector) {
			$('.mcOption').hide().find('select').attr('disabled', true);

			if($(selector).val() == 'Limited Edition Print') {
				$('.mcPrintOption').show().find('select').attr('disabled', false);
			}

			if($(selector).val() == 'Canvas Print') {
				$('.mcCanvasOption').show().find('select').attr('disabled', false);
			}

			if($(selector).val() == 'Pro Floating Print') {
				$('.mcAluOption').show().find('select').attr('disabled', false);
			}

			if($(selector).val() == 'HD Acrylic Print') {
				$('.mcAcrylicOption').show().find('select').attr('disabled', false);
			}

		}

		$('.mcSelector').change(function() {switchOptions($(this));} );
		switchOptions($('.mcSelector'));
	});
